# Musicopy

A simple tool to copy local songs from a `m3u` playlist to an Android device via `adb`.

It's currently as simple as it can get, which also means that it will take a while to copy all files.


### Usage

```bash
./musicopy.rb path/to/playlist.m3u
```


#### Why?

Because the MTP protocol is just too buggy and messed up one too many times for me.
