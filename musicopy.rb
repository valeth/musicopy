#!/usr/bin/env ruby
# frozen_string_literal: true

require "forwardable"
require "thwait"
require "tempfile"
require "yaml"
require "pathname"
require "logger"
require "childprocess"
require "fileutils"


LOG = Logger.new(
  File.open("output.log", "a+"),
  formatter: proc { |sev, date, prog, msg| format("[%5s] %s %s: %s\n", sev, date, prog, msg) },
  datetime_format: "%FT%T",
  level: :debug
)
APP_ROOT = Pathname(__dir__).expand_path.freeze
TMP_PATH = APP_ROOT / "tmp"
OUT_PATH = APP_ROOT / "encoded"
CONFIG = YAML.safe_load((APP_ROOT / "config.yml").read).transform_keys(&:to_sym).freeze
PLAYLIST_PATH = Pathname(ARGV[0]).expand_path.freeze

cpath = Pathname(CONFIG[:collection_path])
COLLECTION_PATH = (cpath.relative? ? Pathname(Dir.home) / cpath : cpath).freeze


module Musicopy
  class QueueIterator
    include Enumerable
    extend Forwardable

    def_delegators :@inner, :push, :close

    def initialize
      @inner = Queue.new
    end

    def each
      yield(@inner.pop) until @inner.empty? && @inner.closed?
    end
  end


  class Encoder
    EncodingError = Class.new(StandardError)

    def initialize(item)
      @options = ["-C", "6"]
      @item = item
      @output_format = CONFIG.fetch(:format, "ogg")
      @tmp_files = []
      @out_file = OUT_PATH / @item.dirname / "#{@item.basename('.*')}.#{@output_format}"
    end

    def encode
      cover_file = @out_file.dirname / "albumart.jpg"
      @out_file.dirname.mkpath

      unless cover_file.exist?
        cover_out = extract_cover
        cover_out.rename(cover_file)
      end

      unless @out_file.exist?
        audio_out = extract_audio
        audio_out.rename(@out_file)
      end

      [@out_file, cover_file]
    rescue FFmpegEncodingError
      LOG.error { "Failed to encode #{@infile}" }
      nil
    ensure
      @tmp_files.each do |f|
        f.unlink if f.exist?
      end
    end

  private

    def extract_audio
      LOG.info { "Encoding audio file for #{@item}" }
      out = tmp_file("audio")
      out_opts = %w[-y -vn -c:a libvorbis -b:a 160k]
      ffmpeg("-i", COLLECTION_PATH / @item, *out_opts, out)
      out
    end

    def extract_cover
      LOG.info { "Extracting cover file for #{@item}" }
      out = tmp_file("cover", "jpg")
      out_opts = %w[-y -an -c:v copy]
      ffmpeg("-i", COLLECTION_PATH / @item, *out_opts, out)
      out
    end

    def tmp_file(name, fmt = @output_format)
      path = Pathname(Tempfile.create([name, ".#{fmt}"], TMP_PATH).path)
      @tmp_files << path
      path
    end

    def ffmpeg(*args)
      process = ChildProcess.build("ffmpeg", *args.map(&:to_s)).tap do |p|
        # p.io.inherit!
        p.start
        p.wait
      end

      raise EncodingError if process.crashed?
    end
  end


  class Device
    ADBError = Class.new(StandardError)

    def initialize(serial_number)
      @serial_number = serial_number
      @collection_path = Pathname(CONFIG[:device_collection_path])
    end

    def file_exists?(path)
      adb("shell", "test -e '#{path}'").last.zero?
    end

    def push(item)
      source_path = item
      target_path = device_item_path(item)
      if file_exists?(target_path)
        LOG.info { "#{target_path} already exists, skipping" }
        return
      end
      LOG.info { "Pushing #{source_path} to #{target_path}" }
      adb!("push", source_path, target_path)
    rescue ADBError => e
      LOG.error { e.message }
    end

    def available?
      stdout, exit_code = adb("devices")
      return false unless exit_code.zero?

      stdout.each_line.drop(1).any? do |line|
        line.chomp.split("\t").first == @serial_number
      end
    end

  private

    def device_item_path(item)
      relative_path = item.each_filename.drop(OUT_PATH.each_filename.count)
      @collection_path / relative_path.join("/")
    end

    def adb(*args)
      rx, tx = IO.pipe

      process = ChildProcess.build("adb", "-s", @serial_number, *args.map(&:to_s)).tap do |p|
        p.io.stdout = tx
        p.start
        p.wait
        tx.close
      end

      [rx, process.exit_code]
    end

    def adb!(*args)
      stdout, exit_code = adb(*args)
      raise ADBError, stdout.read unless exit_code.zero?

      stdout
    end
  end
end


# Entry point
DEVICE = Musicopy::Device.new(CONFIG[:device_serial_number])

TMP_PATH.mkpath
OUT_PATH.mkpath

abort("Device not available") unless DEVICE.available?

encoder_queue = Musicopy::QueueIterator.new
transfer_queue = Musicopy::QueueIterator.new
threads = []

# Playlist reader
threads << Thread.new do
  LOG.info "Reading playlist"

  PLAYLIST_PATH.readlines(chomp: true).each do |item|
    encoder_queue.push(Pathname(item))
  end

  encoder_queue.close
end

# Encoder
threads << Thread.new do
  encoder_queue.each do |item|
    result = Musicopy::Encoder.new(item).encode
    next unless result

    result.each { |x| transfer_queue.push(x) }
  end

  transfer_queue.close
end

# Transfer
threads << Thread.new do
  transfer_queue.each do |item|
    DEVICE.push(item)
  end
end

ThreadsWait.all_waits(*threads)

LOG.info "Finished"
